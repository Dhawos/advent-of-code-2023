use std::iter::zip;

use advent_2023_05::load;

const PUZZLE_NUMBER: u32 = 9;

fn resolve_sequences(input_sequence: Vec<i64>) -> Vec<Vec<i64>> {
    let mut sequences: Vec<Vec<i64>> = Vec::new();
    let mut finished = false;
    sequences.push(input_sequence);
    let mut current_sequence = &sequences[sequences.len() - 1];
    while !finished {
        let first_slice = &current_sequence[0..current_sequence.len()-1];
        let second_slice = &current_sequence[1..current_sequence.len()];
        let new_sequence = first_slice
            .iter()
            .zip(second_slice.iter())
            .map(|(first,second)| {
                second - first
            })
            .collect();
        sequences.push(new_sequence);
        current_sequence = &sequences[sequences.len() - 1];
        if current_sequence.iter().fold(true, |acc,x| acc && *x == 0) {
            finished = true;
        };
    };
    sequences
}

fn extrapolate(input_sequences: &mut Vec<Vec<i64>>) {
    let sequences_count = input_sequences.len()-1;
    input_sequences[sequences_count].push(0); //Add first 0
    for i in (0..sequences_count).rev() {
        let len = input_sequences[i].len(); 
        let sum = input_sequences[i][len-1] + input_sequences[i+1][len-1];
        input_sequences[i].push(sum);
    }
}

fn extrapolate_backwards(input_sequences: &mut Vec<Vec<i64>>) {
    let sequences_count = input_sequences.len()-1;
    input_sequences[sequences_count].insert(0,0); //Add first 0
    for i in (0..sequences_count).rev() {
        let sum = input_sequences[i][0] - input_sequences[i+1][0];
        input_sequences[i].insert(0,sum);
    }
}

fn main() {
    part1();
    part2();
}


fn part1() {
    let input = load("../inputs", PUZZLE_NUMBER, Some("input"));
    let lines = input.lines();
    let reports : Vec<Vec<Vec<i64>>> = lines
        .map(|line| {
            line.split_whitespace().map(|x| x.parse::<i64>().unwrap()).collect()
        })
        .map(|x| resolve_sequences(x))
        .map(|mut x| {extrapolate(&mut x);x})
        .collect();
    let result = reports.iter().fold(0,|acc,x| acc + x.first().unwrap().last().unwrap() );
    dbg!(result);

}

fn part2() {
    let input = load("../inputs", PUZZLE_NUMBER, Some("input"));
    let lines = input.lines();
    let reports : Vec<Vec<Vec<i64>>> = lines
        .map(|line| {
            line.split_whitespace().map(|x| x.parse::<i64>().unwrap()).collect()
        })
        .map(|x| resolve_sequences(x))
        .map(|mut x| {extrapolate_backwards(&mut x);x})
        .collect();
    let result = reports.iter().fold(0,|acc,x| acc + x.first().unwrap().first().unwrap() );
    dbg!(reports);
    dbg!(result);
}
