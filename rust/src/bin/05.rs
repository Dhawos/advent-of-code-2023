use advent_2023_05::load;
use std::{
    cmp::{max, min},
    num::ParseIntError,
    ops::Range,
};

#[derive(Debug)]
enum AlamanacLoadError {
    SeedParseError,
    ParseIntError,
    TransformationMapError,
}

impl From<ParseIntError> for AlamanacLoadError {
    fn from(_err: ParseIntError) -> AlamanacLoadError {
        AlamanacLoadError::ParseIntError
    }
}

#[derive(Debug, Clone)]
enum Seed {
    SingleSeed(i64),
    SeedRange(i64, i64),
}

#[derive(Debug, Eq, PartialEq)]
struct TransformationMapEntry {
    destination_range_start: i64,
    source_range_start: i64,
    range: i64,
}

impl TransformationMapEntry {
    fn new(input: &str) -> Result<TransformationMapEntry, AlamanacLoadError> {
        let mut arguments = input.split(' ');
        let destination_range_start = arguments
            .next()
            .ok_or(AlamanacLoadError::TransformationMapError)?
            .parse::<i64>()?;
        let source_range_start = arguments
            .next()
            .ok_or(AlamanacLoadError::TransformationMapError)?
            .parse::<i64>()?;
        let range = arguments
            .next()
            .ok_or(AlamanacLoadError::TransformationMapError)?
            .parse::<i64>()?;
        Ok(TransformationMapEntry {
            destination_range_start,
            source_range_start,
            range,
        })
    }

    fn contains(&self, source: &i64) -> bool {
        if (self.source_range_start..self.source_range_start + self.range).contains(source) {
            return true;
        }
        return false;
    }

    fn convert(&self, source: &i64) -> i64 {
        if self.contains(source) {
            let offset = source - self.source_range_start;
            self.destination_range_start + offset
        } else {
            *source
        }
    }

    fn convert_range(&self, source_range: &Range<i64>) -> Range<i64> {
        let diff = self.destination_range_start - self.source_range_start;
        let result = (source_range.start + diff)..(source_range.end + diff);
        dbg!(&diff);
        dbg!(&source_range);
        dbg!(&result);
        result
    }
}

#[derive(Debug)]
struct SeedRange {
    index: usize,
    range: Range<i64>,
}

fn distinct(first: &Range<i64>, other: &Range<i64>) -> bool {
    if first.start >= other.end || first.end <= other.start {
        return true;
    }
    false
}

#[derive(Debug)]
struct TransformationMap(Vec<TransformationMapEntry>);

impl TransformationMap {
    fn new() -> TransformationMap {
        TransformationMap(Vec::new())
    }

    fn translate(&self, seed: &i64) -> i64 {
        for map in self.0.iter() {
            if map.contains(seed) {
                return map.convert(seed);
            }
        }
        return *seed;
    }
}

#[derive(Default)]
pub struct Almanac {
    seeds: Vec<Seed>,
    transformations_maps: Vec<TransformationMap>,
}

impl Almanac {
    fn load_from_string(input: String, use_ranges: bool) -> Result<Almanac, AlamanacLoadError> {
        let mut lines = input.lines();
        let first_line = lines.next().ok_or(AlamanacLoadError::SeedParseError)?;
        let mut seeds: Vec<Seed>;
        if !use_ranges {
            seeds = first_line
                .split_whitespace()
                .skip(1)
                .map(|x| x.parse::<i64>().map(|y| Seed::SingleSeed(y)))
                .collect::<Result<Vec<_>, _>>()?;
        } else {
            seeds = Vec::new();
            let test = first_line
                .split_whitespace()
                .skip(1)
                .map(|x| x.parse::<i64>())
                .collect::<Result<Vec<_>, _>>()?;
            let pairs = test.windows(2).step_by(2);
            for pair in pairs {
                seeds.push(Seed::SeedRange(pair[0], pair[1]));
            }
        }
        lines.next();
        lines.next();
        let mut transformations: Vec<TransformationMap> = Vec::new();
        let mut current_transformation_map = TransformationMap::new();
        for line in lines {
            match line.chars().next() {
                None => {}
                Some(c) => {
                    if !c.is_digit(10) {
                        transformations.push(current_transformation_map);
                        current_transformation_map = TransformationMap::new();
                    } else {
                        current_transformation_map
                            .0
                            .push(TransformationMapEntry::new(line)?)
                    }
                }
            };
        }
        transformations.push(current_transformation_map);
        Ok(Almanac {
            seeds,
            transformations_maps: transformations,
        })
    }

    fn resolve_min_location(&self) -> i64 {
        let mut min_location = i64::max_value();
        for seed in self.seeds.iter() {
            match seed {
                Seed::SingleSeed(seed) => {
                    let mut current_seed = *seed;
                    for transformation in &self.transformations_maps {
                        current_seed = transformation.translate(&current_seed);
                    }
                    if current_seed < min_location {
                        min_location = current_seed;
                    }
                }
                Seed::SeedRange(seed, delta) => {
                    let mut queue: Vec<SeedRange> = Vec::new();
                    queue.push(SeedRange {
                        index: 0,
                        range: *seed..*seed + *delta,
                    });
                    while queue.len() > 0 {
                        // Convert range
                        let mut seed_range = queue.pop().unwrap();
                        if seed_range.index == 7 {
                            min_location = min(min_location, seed_range.range.start);
                            dbg!(min_location);
                            continue
                        } else {
                            let transformation_map_entries =
                                &self.transformations_maps[seed_range.index];
                            let mut translated = false;
                            for entry in &transformation_map_entries.0 {
                                let entry_range = entry.source_range_start
                                    ..entry.source_range_start + entry.range;

                                if distinct(&seed_range.range, &entry_range) {
                                    continue;
                                }

                                if seed_range.range.start < entry_range.start {
                                    queue.push(SeedRange {
                                        index: seed_range.index,
                                        range: seed_range.range.start..entry_range.start,
                                    });
                                    seed_range.range.start = entry_range.start
                                }

                                if seed_range.range.end > entry_range.end {
                                    queue.push(SeedRange {
                                        index: seed_range.index,
                                        range: entry_range.end..seed_range.range.end,
                                    });
                                    seed_range.range.end = entry_range.end
                                }

                                if seed_range.range.start >= entry_range.start && seed_range.range.end <= entry_range.end {
                                    // perfect overlap
                                    let new_range = entry.convert_range(&seed_range.range);
                                    queue.push(SeedRange {
                                        index: seed_range.index + 1,
                                        range: new_range,
                                    });
                                    translated = true;
                                    break;
                                }
                            }
                            if !translated{
                                queue.push(SeedRange {
                                    index: seed_range.index + 1,
                                    range: seed_range.range,
                                });
                            }
                        }
                    }
                }
            }
        }
        return min_location;
    }
}

fn main() {
    let input = load("../inputs", 5, Some("input"));
    let almanac = Almanac::load_from_string(input, true).unwrap();
    let result = almanac.resolve_min_location();
    println!("{}", result)
}

#[cfg(test)]
#[test]
fn convert() {
    let test = TransformationMapEntry {
        destination_range_start: 50,
        source_range_start: 98,
        range: 2,
    };
    assert_eq!(test.convert(&98), 50);
    assert_eq!(test.convert(&99), 51);
    assert_eq!(test.convert(&12), 12);
}

#[test]
fn example() {
    let input = load("../inputs", 5, Some("example"));
    let almanac = Almanac::load_from_string(input, false).unwrap();

    assert_eq!(
        almanac.transformations_maps[0].0[0],
        TransformationMapEntry {
            destination_range_start: 50,
            source_range_start: 98,
            range: 2
        }
    );
    assert_eq!(
        almanac.transformations_maps[0].0[1],
        TransformationMapEntry {
            destination_range_start: 52,
            source_range_start: 50,
            range: 48
        }
    );
    let result = almanac.resolve_min_location();
    assert_eq!(result, 35);
}

#[test]
fn first_part() {
    let input = load("../inputs", 5, Some("input"));
    let almanac = Almanac::load_from_string(input, false).unwrap();
    let result = almanac.resolve_min_location();
    assert_eq!(result, 111627841)
}

#[test]
fn second_part_example() {
    let input = load("../inputs", 5, Some("example"));
    let almanac = Almanac::load_from_string(input, true).unwrap();
    let mut expected_result: Vec<i64> = (79..93).collect();
    expected_result.append(&mut (55..68).collect());
    let result = almanac.resolve_min_location();
    assert_eq!(result, 46)
}
