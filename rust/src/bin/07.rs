use core::panic;
use std::{cmp::Ordering, collections::HashMap, u32};

use advent_2023_05::load;

const PUZZLE_NUMBER: u32 = 7;

#[derive(Eq, PartialEq, PartialOrd, Debug)]
enum HandType {
    FiveOfAKind,
    FourOfAKind,
    FullHouse,
    ThreeOfAKind,
    TwoPairs,
    OnePair,
    HighCard,
}

#[derive(Debug)]
struct Hand {
    hand: [u32; 5],
    bid: u64,
    hand_type: HandType,
}

impl Eq for Hand {}

impl PartialEq for Hand {
    fn eq(&self, other: &Self) -> bool {
        self.hand == other.hand
    }
}

impl Ord for Hand {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        if self.hand_type > other.hand_type {
            return Ordering::Greater;
        }
        if other.hand_type > self.hand_type {
            return Ordering::Less;
        }
        // Same hand type
        return other.hand.cmp(&self.hand);
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        return Some(self.cmp(other));
    }
}

impl Hand {
    fn parse_hand_string(cards: &str, joker_enabled: bool) -> [u32; 5] {
        cards
            .chars()
            .map(|c| match c {
                'A' => 14,
                'K' => 13,
                'Q' => 12,
                'J' => {
                    if joker_enabled {
                        1
                    } else {
                        11
                    }
                }
                'T' => 10,
                _ => {
                    if c.is_digit(10) {
                        c.to_digit(10).unwrap()
                    } else {
                        panic!("cannot parse {}", c);
                    }
                }
            })
            .collect::<Vec<u32>>()
            .try_into()
            .unwrap()
    }

    fn new_part1(cards: &str, bid: u64) -> Hand {
        let hand = Self::parse_hand_string(cards, false);
        let mut cards_count: HashMap<u32, u32> = HashMap::new();
        for card in hand {
            match cards_count.get_mut(&card) {
                Some(count) => {
                    *count = *count + 1;
                }
                None => {
                    cards_count.insert(card, 1);
                }
            }
        }
        let mut pairs = 0;
        let mut three_of_a_kind = 0;
        let mut four_of_a_kind = 0;
        let mut five_of_a_kind = 0;
        for (_, count) in cards_count {
            if count == 5 {
                five_of_a_kind = five_of_a_kind + 1;
            }
            if count == 4 {
                four_of_a_kind = four_of_a_kind + 1;
            }
            if count == 3 {
                three_of_a_kind = three_of_a_kind + 1;
            }
            if count == 2 {
                pairs = pairs + 1;
            }
        }
        let hand_type =
            Self::hand_type_from_counts(five_of_a_kind, four_of_a_kind, three_of_a_kind, pairs);
        Hand {
            hand,
            bid,
            hand_type,
        }
    }

    fn new_part2(cards: &str, bid: u64) -> Hand {
        let hand = Self::parse_hand_string(cards, true);
        let mut cards_count: HashMap<u32, u32> = HashMap::new();
        for card in hand {
            match cards_count.get_mut(&card) {
                Some(count) => {
                    *count = *count + 1;
                }
                None => {
                    cards_count.insert(card, 1);
                }
            }
        }
        let mut sorted_card_count: Vec<(&u32, &u32)> = cards_count.iter().collect();
        sorted_card_count.sort_by(|a, b| b.1.cmp(a.1));

        let mut pairs = 0;
        let mut three_of_a_kind = 0;
        let mut four_of_a_kind = 0;
        let mut five_of_a_kind = 0;
        let mut joker_count = *cards_count.get(&1).unwrap_or(&0);
        dbg!(&sorted_card_count);
        dbg!(&joker_count);
        for (card, count) in sorted_card_count {
            if card == &1 && count < &5 {
                // We don't evaluate joker unless we have five of them
                continue;
            }
            if count + joker_count == 5 || (card, count) == (&1, &5) {
                five_of_a_kind = five_of_a_kind + 1;
                break;
            }
            if count + joker_count == 4 {
                four_of_a_kind = four_of_a_kind + 1;
                break;
            }
            if count + joker_count == 3 {
                three_of_a_kind = three_of_a_kind + 1;
                joker_count = joker_count - (3 - count);
                continue;
            }
            if count + joker_count == 2 {
                pairs = pairs + 1;
                joker_count = joker_count - (2 - count);
                continue;
            }
        }
        dbg!((five_of_a_kind, four_of_a_kind, three_of_a_kind, pairs));
        let hand_type =
            Self::hand_type_from_counts(five_of_a_kind, four_of_a_kind, three_of_a_kind, pairs);
        dbg!((&hand, &hand_type));
        Hand {
            hand,
            bid,
            hand_type,
        }
    }

    fn hand_type_from_counts(five: u32, four: u32, three: u32, pairs: u32) -> HandType {
        if five == 1 {
            HandType::FiveOfAKind
        } else if four == 1 {
            HandType::FourOfAKind
        } else if three == 1 {
            if pairs == 1 {
                HandType::FullHouse
            } else {
                HandType::ThreeOfAKind
            }
        } else {
            if pairs == 2 {
                HandType::TwoPairs
            } else if pairs == 1 {
                HandType::OnePair
            } else {
                HandType::HighCard
            }
        }
    }
}

fn main() {
    part1();
    part2();
}

fn part1() {
    let input = load("../inputs", PUZZLE_NUMBER, Some("input"));
    let mut hands = input
        .lines()
        .map(|line| {
            let mut split_line = line.split_whitespace();
            let cards = split_line.next().unwrap();
            let bid = split_line.next().unwrap().parse::<u64>().unwrap();
            Hand::new_part1(cards, bid)
        })
        .collect::<Vec<Hand>>();
    hands.sort();
    let result = hands
        .iter()
        .rev()
        .enumerate()
        .fold(0, |sum, (index, hand)| {
            sum + ((index as u64 + 1) * hand.bid)
        });
    dbg!(&result);
}

fn part2() {
    let input = load("../inputs", PUZZLE_NUMBER, Some("input"));
    let mut hands = input
        .lines()
        .map(|line| {
            let mut split_line = line.split_whitespace();
            let cards = split_line.next().unwrap();
            let bid = split_line.next().unwrap().parse::<u64>().unwrap();
            Hand::new_part2(cards, bid)
        })
        .collect::<Vec<Hand>>();
    hands.sort();
    dbg!(&hands);
    let result = hands
        .iter()
        .rev()
        .enumerate()
        .fold(0, |sum, (index, hand)| {
            sum + ((index as u64 + 1) * hand.bid)
        });
    dbg!(&result);
}
