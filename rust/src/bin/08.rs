use core::panic;
use std::collections::HashMap;
use num::integer::lcm;

use advent_2023_05::load;

const PUZZLE_NUMBER: u32 = 8;

#[derive(Debug)]
enum Direction {
    Left,
    Right,
}

#[derive(Debug)]
struct Instructions {
    current_index: usize,
    instruction_set: Vec<Direction>,
}

impl Instructions {
    fn new(input: &str) -> Instructions {
        let mut instruction_set: Vec<Direction> = Vec::new();
        for char in input.chars() {
            if char == 'L' {
                instruction_set.push(Direction::Left);
            } else if char == 'R' {
                instruction_set.push(Direction::Right);
            } else {
                panic!("Intruction set must be R or L")
            }
        }
        Instructions {
            current_index: 0,
            instruction_set,
        }
    }

    fn next_direction(&mut self) -> &Direction {
        let result = &self.instruction_set[self.current_index];
        self.current_index = self.current_index + 1;
        if self.current_index >= self.instruction_set.len() {
            self.current_index = 0;
        }
        return result
    }
}

fn main() {
    part1();
    part2();
}

fn part1() {
    let input = load("../inputs", PUZZLE_NUMBER, Some("input"));
    let mut lines = input.lines();
    let mut instructions = Instructions::new(lines.next().unwrap());
    let graph = lines.skip(1).map(|line| {
        let mut split_line = line.split(" = ");
        let key = split_line.next().unwrap().trim();
        let mut split_values = split_line.next().unwrap().split(",");
        let value = ( 
            split_values.next().unwrap().replace("(", "").trim().to_string(),
            split_values.next().unwrap().replace(")", "").trim().to_string(),
        );
        (key.to_string(), value)
    }).collect::<HashMap<String,(String,String)>>();
    let mut current_node = "AAA";
    let mut steps = 0;
    while current_node != "ZZZ" {
        steps = steps + 1;
        match instructions.next_direction() {
            Direction::Left => current_node = &graph[current_node].0,
            Direction::Right => current_node = &graph[current_node].1,
        }
    }
    dbg!(steps);
}

fn part2() {
    let input = load("../inputs", PUZZLE_NUMBER, Some("input"));
    let mut lines = input.lines();
    let mut instructions = Instructions::new(lines.next().unwrap());
    let graph = lines.skip(1).map(|line| {
        let mut split_line = line.split(" = ");
        let key = split_line.next().unwrap().trim();
        let mut split_values = split_line.next().unwrap().split(",");
        let value = ( 
            split_values.next().unwrap().replace("(", "").trim().to_string(),
            split_values.next().unwrap().replace(")", "").trim().to_string(),
        );
        (key.to_string(), value)
    }).collect::<HashMap<String,(String,String)>>();
    let mut current_nodes: Vec<&String> = graph.keys().filter(|s| s.ends_with("A")).collect();
    let mut paths_length: Vec<u64> = vec![];
    let mut steps: u64 = 0;
    while current_nodes.len() > 0 {
        steps = steps + 1;
        match instructions.next_direction() {
            Direction::Left => for node in current_nodes.iter_mut() {*node = &graph[*node].0},
            Direction::Right => for node in current_nodes.iter_mut() {*node = &graph[*node].1},
        }
        for _ in current_nodes.iter().filter(|node| node.ends_with("Z")) {
            paths_length.push(steps);
        }
        current_nodes.retain(|node| !node.ends_with("Z"));
    }
    let result = paths_length.iter().fold(1,|acc,x| lcm(acc, *x) );

    dbg!(result);

}
