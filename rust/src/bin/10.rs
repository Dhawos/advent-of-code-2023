use core::panic;
use std::{borrow::{Borrow, BorrowMut}, cell::RefCell, fmt::Debug, ops::Deref, rc::Rc, thread::current};

use advent_2023_05::load;

const PUZZLE_NUMBER: u32 = 10;

#[derive(Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    fn next(point: (usize,usize), direction: Direction) -> (usize,usize) {
        match  direction {
            Direction::Up => (point.0,point.1+1),
            Direction::Down => (point.0,point.1-1),
            Direction::Left => (point.1-1,point.1),
            Direction::Right => (point.1+1,point.1),
        } 
    } 

    fn previous(point: (usize,usize), direction: Direction) -> (usize,usize) {
        match  direction {
            Direction::Up => (point.0,point.1-1),
            Direction::Down => (point.0,point.1+1),
            Direction::Left => (point.1+1,point.1),
            Direction::Right => (point.1-1,point.1),
        } 
    } 

    const VALUES: [Self;4] = [Self::Up,Self::Down,Self::Left,Self::Right];
}

#[derive(Debug)]
enum NodeType {
    Starting,
    Vertical,
    Horizontal,
    UpRight,
    UpLeft,
    DownLeft,
    DownRight,
    Ground,
}

impl NodeType {
    fn directions(&self) -> Option<(Direction,Direction)> {
        match self {
            NodeType::Starting => None, 
            NodeType::Vertical => Some((Direction::Up,Direction::Down)),
            NodeType::Horizontal => Some((Direction::Left,Direction::Right)),
            NodeType::UpRight => Some((Direction::Up,Direction::Right)),
            NodeType::UpLeft => Some((Direction::Up,Direction::Left)),
            NodeType::DownLeft => Some((Direction::Down,Direction::Left)),
            NodeType::DownRight => Some((Direction::Up,Direction::Right)), 
            NodeType::Ground => None,
        }
    }
}

struct Node {
    node_type: NodeType,
    id: (usize,usize),
    visited: bool,
    neighbours: Vec<Rc<RefCell<Self>>>,
}

impl Debug for Node {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?} - [{:?} - {:?}] - visited: {:?}", self.id,self.neighbours[0].deref().borrow().id,self.neighbours[1].deref().borrow().id,self.visited);
        Ok(())
    }
}


fn main() {
    part1();
    part2();
}


fn part1() {
    let input = load("../inputs", PUZZLE_NUMBER, Some("input"));
    let mut starting_point = (0,0);
    let mut map : Vec<Vec<Rc<RefCell<Node>>>> = Vec::new();
    for (i,line) in input.lines().enumerate() {
        map.push(vec![] as Vec<Rc<RefCell<Node>>>);
        for (j,char) in line.chars().enumerate() {
            let node_type = match char {
                '|' => NodeType::Vertical,
                '-' => NodeType::Horizontal,
                'L' => NodeType::UpRight,
                'J' => NodeType::UpLeft,
                '7' => NodeType::DownLeft,
                'F' => NodeType::DownRight,
                '.' => NodeType::Ground,
                'S' => NodeType::Starting,
                _ => panic!("unknown character"),
            };
            if char == 'S' {
                starting_point = (i,j);
            }
            map[i].push(Rc::new(RefCell::new(Node{node_type,id: (i,j),visited: false,neighbours: Vec::new()})));
        }
    }
    
    // compute neighbours 
    for (i,line) in map.iter().enumerate() {
        for (j,node_ref) in line.iter().enumerate() {
            let mut node = node_ref.deref().borrow_mut();
            match node.node_type {
                NodeType::Vertical|NodeType::UpRight|NodeType::UpLeft => {if i >= 1 {node.neighbours.push(map[i-1][j].clone())}},
                _ => {},
            }
            match node.node_type {
                NodeType::Vertical|NodeType::DownRight|NodeType::DownLeft=> {if i+1 < line.len() {node.neighbours.push(map[i+1][j].clone())}},
                _ => {},
            }
            match node.node_type {
                NodeType::Horizontal|NodeType::UpLeft|NodeType::DownLeft=> {if j >= 1 {node.neighbours.push(map[i][j-1].clone())}},
                _ => {},
            }
            match  node.node_type {
                NodeType::Horizontal|NodeType::UpRight|NodeType::DownRight=> {if j+1 < line.len() {node.neighbours.push(map[i][j+1].clone())}},
                _ => {},
            }
            for neighbour in &node.neighbours {
                let mut neighbour = neighbour.deref().borrow_mut();
                match neighbour.node_type {
                    NodeType::Starting => {neighbour.neighbours.push(node_ref.clone())},
                    _ => {},
                    
                }
            }
        }
    }

    map[starting_point.0][starting_point.1].deref().borrow_mut().visited = true;
    let mut current_nodes = ((*map[starting_point.0][starting_point.1]).borrow().neighbours[0].clone()
                             ,(*map[starting_point.0][starting_point.1]).borrow().neighbours[1].clone());
    let mut current_ids = ((*current_nodes.0).borrow().id,(*current_nodes.1).borrow().id);
    let mut count = 1;
    while current_ids.0 != current_ids.1 {
        current_nodes.0.deref().borrow_mut().visited = true;
        current_nodes.1.deref().borrow_mut().visited = true;
        dbg!((*current_nodes.0).borrow());
        let new_node_0 = current_nodes.0.deref().borrow().neighbours.iter().filter(|node|  {
            dbg!((*node).deref().borrow());
            dbg!((*node).deref().borrow().visited);
            dbg!((*node).deref().borrow().id);
            !(*node)
                .deref()
                .borrow()
                .visited
            })
            .next()
            .unwrap()
            .clone();
        let new_node_1 = current_nodes.1.deref().borrow().neighbours.iter().filter(|node| !(*node).deref().borrow().visited).next().unwrap().clone();
        current_nodes.0 = new_node_0; 
        current_nodes.1 = new_node_1; 
        current_ids = (current_nodes.0.deref().borrow().id,current_nodes.1.deref().borrow().id);
        count = count + 1;
    }
    dbg!(count);
}

fn part2() {
}
