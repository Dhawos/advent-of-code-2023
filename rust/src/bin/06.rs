use core::f64;
use std::ops::Range;

use advent_2023_05::load;

const PUZZLE_NUMBER: u32 = 6;

#[derive(Debug)]
struct Race {
    time: u64,
    distance_record: u64,
}

impl Race {
    fn winning_times(&self) -> Vec<u64> {
        let mut result: Vec<u64> = Vec::new();
        for held_time in 1..self.time {
            let travel_time = self.time - held_time;
            let speed = held_time;
            let traveled_dinstance = travel_time * speed;
            if traveled_dinstance > self.distance_record {
                result.push(held_time)
            }
        }
        return result;
    }

    fn winning_times_part2(&self) -> Range<u64> {
        let lower_bound: u64;
        let upper_bound: u64;
        let mut considered_range = 1..self.time / 2;
        while considered_range.end - considered_range.start > 1 {
            dbg!(&considered_range);
            let mean_held_time = (considered_range.start + considered_range.end) / 2;
            let travel_time = self.time - mean_held_time;
            let speed = mean_held_time;
            let traveled_dinstance = travel_time * speed;
            if traveled_dinstance > self.distance_record {
                considered_range.end = mean_held_time;
            } else {
                considered_range.start = mean_held_time;
            }
        }
        lower_bound = considered_range.start + 1; // Account for truncation
        considered_range = self.time / 2..self.time;
        while considered_range.end - considered_range.start > 1 {
            let mean_held_time = (considered_range.start + considered_range.end) / 2;
            let travel_time = self.time - mean_held_time;
            let speed = mean_held_time;
            let traveled_dinstance = travel_time * speed;
            if traveled_dinstance > self.distance_record {
                considered_range.start = mean_held_time;
            } else {
                considered_range.end = mean_held_time;
            }
        }
        upper_bound = considered_range.start;
        return lower_bound..upper_bound + 1; // Account for exclusion in range type
    }
}

fn main() {
    first_part();
    second_part();
}

fn first_part() {
    let input = load("../inputs", PUZZLE_NUMBER, Some("input"));
    let lines: Vec<&str> = input.lines().collect();
    let times: Vec<u64> = lines[0]
        .split_whitespace()
        .skip(1)
        .map(|time| time.parse::<u64>().unwrap())
        .collect();
    let records: Vec<u64> = lines[1]
        .split_whitespace()
        .skip(1)
        .map(|time| time.parse::<u64>().unwrap())
        .collect();
    let mut races: Vec<Race> = Vec::new();
    for i in 0..times.len() {
        races.push(Race {
            time: times[i],
            distance_record: records[i],
        });
    }

    // Solution
    let mut results: Vec<Vec<u64>> = Vec::new();
    for race in races {
        results.push(race.winning_times());
    }
    let result = results.iter().fold(1, |acc, x| acc * x.len());
    dbg!(result);
}

fn second_part() {
    let input = load("../inputs", PUZZLE_NUMBER, Some("input"));
    let lines: Vec<&str> = input.lines().collect();
    let time = lines[0]
        .trim_start_matches("Time:")
        .replace(" ", "")
        .parse::<u64>()
        .unwrap();
    let distance_record = lines[1]
        .trim_start_matches("Distance:")
        .replace(" ", "")
        .parse::<u64>()
        .unwrap();
    let race = Race {
        time,
        distance_record,
    };
    let range = race.winning_times_part2();
    let result = range.end - range.start;
    dbg!(result);
}
