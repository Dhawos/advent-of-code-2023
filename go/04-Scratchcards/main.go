package main

import (
	"math"
	"strings"

	"gitlab.com/Dhawos/advent-of-code-2023/utils"
)

type Card struct {
	id                int
	winningNumbersMap map[string]int
	actualNumbers     []string
	nbWinningNumbers  int
}

func NewCard(id int, winningNumberMap map[string]int, actualNumbers []string) *Card {
	nbWinningNumbers := 0
	for _, number := range actualNumbers {
		_, exists := winningNumberMap[number]
		if exists {
			nbWinningNumbers = nbWinningNumbers + 1
		}
	}
	return &Card{
		id:                id,
		winningNumbersMap: winningNumberMap,
		actualNumbers:     actualNumbers,
		nbWinningNumbers:  nbWinningNumbers,
	}
}

func (c *Card) score() int {
	if c.nbWinningNumbers == 0 {
		return 0
	}
	return int(math.Pow(2, float64(c.nbWinningNumbers)-1))
}

func partTwo(cards []*Card) int {
	cardsStack := make([]*Card, len(cards))
	copy(cardsStack, cards)
	count := 0
	for len(cardsStack) > 0 {
		count = count + 1
		card := cardsStack[0]
		cardsStack = cardsStack[1:]
		// fmt.Printf("Card %d\n", card.id)
		for i := 1; i <= card.nbWinningNumbers; i++ {
			cardsStack = append(cardsStack, cards[card.id+i-1])
		}
	}
	return count
}

func main() {
	input := utils.ReadInput("input.txt")
	sum := 0
	cards := make([]*Card, 0)
	for i, line := range strings.Split(strings.TrimRight(input, "\n"), "\n") {
		actualLine := strings.Split(line, ":")[1]
		actualLine = strings.ReplaceAll(actualLine, "  ", " ")
		winningNumbers := strings.Split(strings.Trim(strings.Split(actualLine, "|")[0], " "), " ")
		actualNumbers := strings.Split(strings.Trim(strings.Split(actualLine, "|")[1], " "), " ")
		winningNumbersMap := map[string]int{}
		for _, number := range winningNumbers {
			winningNumbersMap[number] = 0
		}
		card := NewCard(i+1, winningNumbersMap, actualNumbers)
		cards = append(cards, card)
		sum = sum + card.score()
	}
	print(sum)
	print("\n")
	print(partTwo(cards))
	print("\n")
}
