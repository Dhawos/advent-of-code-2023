package main

import (
	"strings"
	"unicode"

	"gitlab.com/Dhawos/advent-of-code-2023/utils"
)

func main() {
	input := utils.ReadInput("input.txt")
	partOne(input)
	partTwo(input)
}

func partOne(input string) {
	sum := 0
	for _, line := range strings.Split(strings.TrimRight(input, "\n"), "\n") {
		firstDigit, lastDigit := -1, -1
		for _, char := range line {
			if unicode.IsDigit(char) {
				if firstDigit == -1 {
					firstDigit = int(char - '0')
				}
				if firstDigit != -1 {
					lastDigit = int(char - '0')
				}
			}
		}
		lineNumber := (firstDigit * 10) + lastDigit
		sum = sum + lineNumber
	}
	println(sum)
}

func partTwo(input string) {
	sum := 0
	for _, line := range strings.Split(strings.TrimRight(input, "\n"), "\n") {
		firstDigit, lastDigit := -1, -1

		for pos, char := range line {
			number := -1
			number, found := findNumber(line[pos:])
			if !found && unicode.IsDigit(char) {
				number = int(char - '0')
			}
			if number != -1 {
				if firstDigit == -1 {
					firstDigit = number
				} else if firstDigit != -1 {
					lastDigit = number
				}
			}
		}
		if firstDigit != -1 && lastDigit != -1 {
			lineNumber := (firstDigit * 10) + lastDigit
			sum = sum + lineNumber
		} else {
			lineNumber := (firstDigit * 10) + firstDigit
			sum = sum + lineNumber
		}
	}
	println(sum)
}

func findNumber(input string) (int, bool) {
	spelledNumbers := map[string]int{
		"one":   1,
		"two":   2,
		"three": 3,
		"four":  4,
		"five":  5,
		"six":   6,
		"seven": 7,
		"eight": 8,
		"nine":  9,
	}
	for spelledNumber, intNumber := range spelledNumbers {
		if len(spelledNumber) <= len(input) && spelledNumber == input[:len(spelledNumber)] {
			return intNumber, true
		}
	}
	return -1, false
}
