package main

import (
  "fmt"
	"strings"
  "strconv"
	"gitlab.com/Dhawos/advent-of-code-2023/utils"
)

const MAX_RED_CUBES = 12
const MAX_GREEN_CUBES = 13
const MAX_BLUE_CUBES = 14

type Draw struct {
  Red int
  Green int
  Blue int
}

func (d Draw) Validate() bool {
  if d.Red > MAX_RED_CUBES {
    return false
  }
  if d.Green > MAX_GREEN_CUBES {
    return false
  }
  if d.Blue > MAX_BLUE_CUBES {
    return false
  }
  return true
}

type Game struct {
  ID int
  Draws []Draw
  MinRed int
  MinGreen int
  MinBlue int
}

func (g Game) MinPower() int {
  return g.MinRed * g.MinGreen * g.MinBlue
}

func (g Game) Validate() bool {
  valid := true
  str := fmt.Sprintf("%d:",g.ID) 
  for _, draw := range g.Draws {
    if draw.Validate() {
      str = str + fmt.Sprintf("%dr%dg%db+ ",draw.Red,draw.Green,draw.Blue)
    } else {
      str = str + fmt.Sprintf("%dr%dg%dbX ",draw.Red,draw.Green,draw.Blue)
      valid = false
    }
  }
  str = str + fmt.Sprintf(" - Minimums : %dr%dg%db",g.MinRed,g.MinGreen,g.MinBlue)
  str = str + "\n"
  print(str)
  return valid 
}

func parseInput() []Game{
	input := utils.ReadInput("input.txt")
  games := make([]Game,0)
	for id, line := range strings.Split(strings.TrimRight(input, "\n"), "\n") {
    draws := make([]Draw,0)
    newGame := Game{
      ID: id+1,
      Draws: draws,
    }
    lineWithoutGame := strings.Split(line,":")[1]
    stringDraws := strings.Split(lineWithoutGame,";")
    for _, stringDraw := range stringDraws {
      draw := Draw{}
      stringColors := strings.Split(stringDraw,",")
      for _, stringColor := range stringColors {
        splitColor := strings.Split(stringColor," ")
        if len(splitColor) != 3 {
          fmt.Printf("error when parsing color, length was %d, should have been 3",len(splitColor))
          fmt.Print(line)
        }
        amount := splitColor[1]
        color := splitColor[2]
        switch color {
	      case "red":
          draw.Red, _ = strconv.Atoi(amount)
          if newGame.MinRed < draw.Red {
            newGame.MinRed = draw.Red
          }
	      case "green":
          draw.Green, _ = strconv.Atoi(amount)
          if newGame.MinGreen < draw.Green {
            newGame.MinGreen = draw.Green
          }
	      case "blue":
          draw.Blue, _ = strconv.Atoi(amount)
          if newGame.MinBlue < draw.Blue {
            newGame.MinBlue = draw.Blue
          }
        }
      }
      newGame.Draws = append(newGame.Draws, draw)
    }
    games = append(games, newGame)
  }
  return games
}

func main() {
  games := parseInput()
  part1Sum := 0
  part2Sum := 0
  for _, game := range games {
    if game.Validate() {
      part1Sum = part1Sum + game.ID
    }
    part2Sum = part2Sum + game.MinPower()
  }
  print(part1Sum)
  print("\n")
  print(part2Sum)
}
