package main

import (
	"fmt"
	"strconv"
	"strings"
	"unicode"

	"gitlab.com/Dhawos/advent-of-code-2023/utils"
)

type Gear struct {
	numbers []int
}

type GearCoordinates struct {
	x int
	y int
}

func isValidSymbol(char rune) bool {
	return char != '.' && !unicode.IsDigit(char)
}

func main() {
	input := utils.ReadInput("input.txt")
	lines := strings.Split(strings.TrimRight(input, "\n"), "\n")
	gears := make([][]*Gear, len(lines))
	for i := range gears {
		gears[i] = make([]*Gear, len(lines[0]))
		for j := range gears[i] {
			gears[i][j] = &Gear{
				numbers: make([]int, 0),
			}
		}
	}
	sum := 0
	for x, line := range lines {
		for y := 0; y < len(line); y++ {
			char := rune(line[y])
			if char == '.' {
			} else if unicode.IsDigit(char) {
				var numberString string
				symbolAdjencent := false
				gearsToAdd := make([]GearCoordinates, 0)
				gearAdded := false
				for ; y < len(line) && unicode.IsDigit(rune(line[y])); y++ {
					numberString = numberString + string(line[y])
					for i := -1; i <= 1; i++ {
						for j := -1; j <= 1; j++ {
							if x+i > 0 && y+j > 0 && x+i < len(lines) && y+j < len(lines[x+i]) {
								currentChar := rune(lines[x+i][y+j])
								if isValidSymbol(currentChar) {
									symbolAdjencent = true
									if currentChar == '*' && !gearAdded {
										gearsToAdd = append(gearsToAdd, GearCoordinates{x: x + i, y: y + j})
										gearAdded = true
									}
								}
							}
						}
					}
				}
				number, _ := strconv.ParseInt(numberString, 10, 0)
				for _, coordinates := range gearsToAdd {
					gear := gears[coordinates.x][coordinates.y]
					fmt.Printf("adding %d to gear %d-%d\n", number, coordinates.x, coordinates.y)
					gear.numbers = append(gear.numbers, int(number))
				}
				if symbolAdjencent {
					sum = sum + int(number)
				}
			}
		}
	}
	print("\n")
	print(sum)
	print("\n")
	part2Sum := 0
	for i := range gears {
		for j := range gears[i] {
			if len(gears[i][j].numbers) == 2 {
				// fmt.Printf("%d-%d\n", i, j)
				part2Sum = part2Sum + (gears[i][j].numbers[0] * gears[i][j].numbers[1])
			}
		}
	}
	print(part2Sum)
}
